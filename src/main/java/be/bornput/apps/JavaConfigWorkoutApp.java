package be.bornput.apps;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import be.bornput.config.SportConfig;
import be.bornput.roles.Coach;
import be.bornput.roles.SwimCoach;


public class JavaConfigWorkoutApp {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		Coach mySwimCoach = context.getBean("swimCoach",SwimCoach.class);

		System.out.println(mySwimCoach.getDailyWorkout());
		System.out.println(mySwimCoach.getDailyFortune());
		System.out.println(mySwimCoach.getTeam());
		System.out.println(mySwimCoach.getEmailAddress());
		
		context.close();
	}
}
