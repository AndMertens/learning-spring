package be.bornput.apps;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import be.bornput.roles.Coach;

public class AnnotationWorkoutApp {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("spring/annotationAppContext.xml") ;
		
		Coach myTennisCoach = context.getBean("sillyCoach",Coach.class);
				
		System.out.println(myTennisCoach.getDailyWorkout());
		System.out.println(myTennisCoach.getDailyFortune());
		
		context.close();
	}
}
