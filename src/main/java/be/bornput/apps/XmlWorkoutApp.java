package be.bornput.apps;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import be.bornput.roles.Coach;

public class XmlWorkoutApp {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring/applicationContext.xml");

		Coach myTennisCoach = context.getBean("sillyCoach", Coach.class);

		System.out.println(myTennisCoach.getDailyWorkout());
		System.out.println(myTennisCoach.getDailyFortune());

		context.close();
	}
}
