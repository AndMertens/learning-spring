package be.bornput.services;

import java.util.Random;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {

	String[] thisFortunes = new String [3]; 

	@Override
	public String getDailyFortune() {
		return getRandomFortune(thisFortunes);
	}

	@PostConstruct
	public void FillFortuneArray(){
		thisFortunes[0] = "Get on!";
		thisFortunes[1] = "Go gor it!";
		thisFortunes[2] = "Almost there!";
	}
	
	public String getRandomFortune(String[] fortunes) {

		Random randomNr = new Random();
		return thisFortunes[randomNr.nextInt(fortunes.length)];
	}
	
	@PreDestroy
	public void doSomeCleanup(){
		thisFortunes = null;
		if (thisFortunes == null )System.out.println("Clear Array");
		
	}

}
