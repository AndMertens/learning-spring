package be.bornput.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("myFortuneService")
public class HappyFortuneService implements FortuneService {

	@Override
	public String getDailyFortune() {
		return ("You will be ready for this");
	}


}
