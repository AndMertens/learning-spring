package be.bornput.roles;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import be.bornput.services.FortuneService;

@Component
public class SwimCoach implements Coach {

	private FortuneService swimfortune;
	
	@Value("${foo.email}")
	private String emailAddress;
	
	@Value("${foo.team}")
	private String team;
	
	public SwimCoach(FortuneService thisSwimFortune){
		swimfortune= thisSwimFortune;
	}
	
	
	@Override
	public String getDailyWorkout() {
		return ("Give me 25 lanes on crawl");
	}

	@Override
	public String getDailyFortune() {
		
		return swimfortune.getDailyFortune();
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getTeam() {
		return team;
	}


	public void setTeam(String team) {
		this.team = team;
	}

}
