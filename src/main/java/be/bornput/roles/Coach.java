package be.bornput.roles;

public interface Coach {

	public String getDailyWorkout();
	public String getDailyFortune();
	public String getTeam();
	public String getEmailAddress();
	
}
