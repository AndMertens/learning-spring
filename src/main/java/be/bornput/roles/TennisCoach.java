package be.bornput.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import be.bornput.services.FortuneService;
import be.bornput.services.HappyFortuneService;



@Component("sillyCoach")
public class TennisCoach implements Coach {
	
	private String emailAddress;
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTeam() {
		return Team;
	}

	public void setTeam(String team) {
		Team = team;
	}

	private String Team;
	
	@Autowired
	private HappyFortuneService fortune;
	
	public void setFortuneService(HappyFortuneService fortune) {
		this.fortune = fortune;
	}

	@Override
	public String getDailyWorkout() {
		return ("Do some backhand volleys");
	}
	
	@Override
	public String getDailyFortune(){
		return fortune.getDailyFortune();
	}
	
	

}
