package be.bornput.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import be.bornput.roles.Coach;
import be.bornput.roles.SwimCoach;
import be.bornput.services.FortuneService;
import be.bornput.services.SadFortuneService;

@Configuration
@PropertySource("classpath:sport.properties")
@ComponentScan("bornput.be.*")
public class SportConfig {

	@Bean
	public FortuneService swimFortuneService(){
		SadFortuneService myswimfortune = new SadFortuneService();
		return myswimfortune;
	}
	
	@Bean
	public Coach swimCoach(){
		SwimCoach mySwimCoach = new SwimCoach(swimFortuneService());
		return mySwimCoach;
	}
	
	
}
